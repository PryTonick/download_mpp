import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {

    private static String INPUT_OF_FILE   = "src\\inFile.txt";
    private static String OUTPUT_OF_FILE  = "src\\outFile.txt";
    private static String PATH_TO_IMG     = "picture";
    private static String PATH_TO_MUSIC   = "music";
    private static String REGEX_FOR_MUSIC = "\\s*(?<=data-url\\s?=\\s?\")[^>]*\\/*(?=\")";
    private static String REGEX_FOR_IMG   = "\\s*(?<=<img [^>]{0,100}src\\s?=\\s?\")[^>\"]+";
    private static String REGEX = "";

    public static void main(String[] args) {

        try (BufferedReader inFile = new BufferedReader(new FileReader(INPUT_OF_FILE));
             BufferedWriter outFile = new BufferedWriter(new FileWriter(OUTPUT_OF_FILE))) {
            extractionOfLinks(inFile, outFile);
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        try (BufferedReader file = new BufferedReader(new FileReader(OUTPUT_OF_FILE))) {
            download(file);
            play();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static void extractionOfLinks(BufferedReader inFile, BufferedWriter outFile)
            throws IOException {

            String link;
            int count = 0 , links = 1;
            String[] array = new String[4];
            while((link = inFile.readLine()) != null) {
                for (String string : link.split(" ")) {
                    array[count] = string;
                    count++;
                }

                choice_of_A_RegEx(links);

                URL url = new URL(array[0]);
                String result;
                try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {
                    result = bufferedReader.lines().collect(Collectors.joining("\n"));
                    Pattern email_pattern = Pattern.compile(REGEX);
                    Matcher matcher = email_pattern.matcher(result);
                    int i = 0;
                    while (matcher.find() && i < 1) {
                        outFile.write(matcher.group() + "\r\n");
                        i++;
                    }
                }
                count = 0;
                links++;
            }
    }

    private static void download(BufferedReader file) {
        String img;
        int count = 0;
        try {
            while ((img = file.readLine()) != null) {
                if (count == 0) {
                    downloadUsingNIO(img, PATH_TO_IMG + String.valueOf(count) + ".jpg");
                    count++;
                } else {
                    downloadUsingNIO(img, PATH_TO_MUSIC + String.valueOf(count) + ".mp3");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void downloadUsingNIO(String strUrl, String file) throws IOException {
        URL url = new URL(strUrl);
        ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
        FileOutputStream stream = new FileOutputStream(file);
        stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
        stream.close();
        byteChannel.close();
    }

    public static void play() {
        try (FileInputStream inputStream = new FileInputStream(
                "C:\\Programs Gorshka\\Programming\\IntelliJ IDEA\\Java coding\\DownLoad_MPP\\" +
                PATH_TO_MUSIC + "1.mp3")) {
            try {
                Player player = new Player(inputStream);
                player.play();
            } catch (JavaLayerException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void choice_of_A_RegEx(int count){
        int choise = count;
        switch (choise){
            case 1:
                REGEX = REGEX_FOR_IMG;
            break;
            case 2:
                REGEX = REGEX_FOR_MUSIC;
            break;
        }
    }
}
